import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import injectTapEventPlugin from "react-tap-event-plugin";
injectTapEventPlugin();


var App = React.createClass({
  render: function() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <a href="#" onTouchTap={this.handleTouchTap} onClick={this.handleClick}>Tap</a>
        <p className="App-intro">
          Hehedit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  },
 
  handleClick: function(e) {
    console.log("click", e);
  },
 
  handleTouchTap: function(e) {
    console.log("touchTap", e);
  }
});
export default App;
